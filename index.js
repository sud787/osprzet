var express = require('express');
var bodyParser = require('body-parser');
const nodemailer = require('nodemailer');
// create application/x-www-form-urlencoded parser
var urlencodedParser = bodyParser.urlencoded({ extended: false })
var app = express();
//console.log('coming here');
app.use('/css', express.static('css')
);

app.use('/fonts', express.static('fonts')
);

app.use('/img', express.static('img')
);
app.use('/js', express.static('js')
);
app.get('/', function (req, res) {
    res.sendFile(__dirname + '/index.html');

});
app.get('/about', function (req, res) {
    res.sendFile(__dirname + '/about.html');

});
app.get('/products', function (req, res) {
    res.sendFile(__dirname + '/products.html');

});
app.get('/services', function (req, res) {
    res.sendFile(__dirname + '/services.html');

});
app.get('/google', function (req, res) {
    res.sendFile(__dirname + '/google.html');

});
app.get('/retargeting', function (req, res) {
    res.sendFile(__dirname + '/retargeting.html');

});

app.get('/social-media-marketting', function (req, res) {
    res.sendFile(__dirname + '/mediaMarketting.html');

});

app.get('/mobile-app-development', function (req, res) {
    res.sendFile(__dirname + '/mobiledevelopment.html');

});
app.listen(process.env.PORT ||3000);